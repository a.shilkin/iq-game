const secondButtonOpen = document.querySelectorAll('.secondButtonOpen');
const downLayer = document.querySelector('.down-layer');
const wrapSeventhBlockSliderSynch = document.querySelector('.wrapSeventhBlockSliderSynch');
const swiperButtonPrev = document.querySelector('.swiperButtonPrev');
const swiperButtonNext = document.querySelector('.swiperButtonNext');
const buttonClose = document.querySelectorAll('.button-close');

for (var i = 0; i < secondButtonOpen.length; i++) {
    secondButtonOpen[i].onclick = function () {
        downLayer.style.display = 'block';
        wrapSeventhBlockSliderSynch.style.display = 'flex';
        swiperButtonPrev.style.display = 'block';
        swiperButtonNext.style.display = 'block';
        return false;
    }
}

for (var i = 0; i < buttonClose.length; i++) {
    buttonClose[i].onclick = function () {
        downLayer.style.display = 'none';
        wrapSeventhBlockSliderSynch.style.display = 'none';
        swiperButtonPrev.style.display = 'none';
        swiperButtonNext.style.display = 'none';
        return false;
    }
}

